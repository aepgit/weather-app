import { Component, OnInit } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, tap } from "rxjs/operators";
import { isApiError } from "./models/api-error";
import { WeatherData } from "./models/weather-data";
import { ApiService } from "./services/api.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
  weatherData$?: Observable<WeatherData | undefined>;

  locationString: string = "Helsinki, Finland";

  errorMsg?: string;

  constructor(private apiService: ApiService) {}

  ngOnInit() {
    this.loadWeatherData();
  }

  loadWeatherData() {
    this.weatherData$ = this.apiService
      .getWeatherData(this.locationString)
      .pipe(
        tap({
          complete: () => {
            this.errorMsg = undefined;
          },
        }),
        catchError((error) => {
          if (isApiError(error.error)) this.errorMsg = error.error.message;
          else this.errorMsg = error.message;

          return of(undefined);
        })
      );
  }
}
