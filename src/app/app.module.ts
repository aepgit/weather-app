import { HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppComponent } from "./app.component";
import { CodeToIconUrlPipe } from "./pipes/code-to-icon-url.pipe";
import { FormsModule } from "@angular/forms";
import { WeatherDisplayComponent } from "./components/weather-display/weather-display.component";

@NgModule({
  declarations: [AppComponent, CodeToIconUrlPipe, WeatherDisplayComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
