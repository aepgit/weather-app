import { Component, Input } from "@angular/core";
import { WeatherData } from "src/app/models/weather-data";

@Component({
  selector: "app-weather-display",
  templateUrl: "./weather-display.component.html",
  styleUrls: ["./weather-display.component.scss"],
})
export class WeatherDisplayComponent {
  @Input() data?: WeatherData;
}
