export interface ApiError {
  cod: string;
  message: string;
}

export const isApiError = (p: any): p is ApiError =>
  p.hasOwnProperty("cod") && p.hasOwnProperty("message");
