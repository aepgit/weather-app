import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "codeToIconUrl",
})
export class CodeToIconUrlPipe implements PipeTransform {
  iconBaseUrl: string = "http://openweathermap.org/img/wn/";

  transform(value: string): unknown {
    return `${this.iconBaseUrl}${value}.png`;
  }
}
