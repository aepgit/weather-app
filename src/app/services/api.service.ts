import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { WeatherData } from "../models/weather-data";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  constructor(private http: HttpClient) {}

  getWeatherData(location: string): Observable<WeatherData> {
    return this.http.get<WeatherData>(
      `${environment.apiUrl}/weather?q=${location}&APPID=${environment.apiKey}&units=metric`
    );
  }
}
